import java.util.Scanner;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Streams {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in, StandardCharsets.UTF_8).useDelimiter("[^\\p{L}\\d]+");
        Map<String, Integer> wordFrequency = new HashMap<>(); // словарь для подсчёта частот слов
        scanner.forEachRemaining(word ->
                wordFrequency.merge(word.toLowerCase(), 1, Integer::sum)); // подсчёт частот
        scanner.close();

        wordFrequency.entrySet().stream() // получаем стрим в формате (слово, частота)
                .sorted((a, b) -> Objects.equals(a.getValue(), b.getValue()) ? // если слова имеют одинаковую частоту, то сравниваем лексикографически
                        a.getKey().compareTo(b.getKey()) : b.getValue().compareTo(a.getValue())) // иначе сравниваем частоты
                .limit(10) // берём первые 10 пар
                .map(Map.Entry::getKey).forEach(System.out::println); // вывод слов
    }
}
